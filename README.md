# HySortOD

Outlier Detection with Sorted Hypercubes.

This implementation is part of the paper submitted to the Knowledge-Based Systems Journal entitled "HySortOD: Fast and Scalable Outlier Detection with Sorted Hypercubes".

### Build

```sh
mvn package
```

### Run

```sh
java -jar target/hysortod-0.0.1-SNAPSHOT.jar datasets/http.csv 3 5 100 1
```

