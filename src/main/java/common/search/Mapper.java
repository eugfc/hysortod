package common.search;

import common.Hypercube;

public interface Mapper {
	public Mapper buildMap(Hypercube[] H);
	public int[] getNeighborhoodDensities();
	public double getMaxDensity();
}
